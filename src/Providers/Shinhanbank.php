<?php
namespace Dayone\Issuer;

class Shinhanbank {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\ShinhanbankServiceProvider');
        return 'Shinhanbank::index';
    }

}