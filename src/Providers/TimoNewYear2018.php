<?php
namespace Dayone\Issuer;

class TimoNewYear2018 {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\TimoServiceProvider');
        return 'Timo::new_year_2018';
    }

}