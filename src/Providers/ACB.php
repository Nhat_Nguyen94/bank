<?php
namespace Dayone\Issuer;

class ACB {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\ACBServiceProvider');
        return 'ACB::index';
    }

}