<?php
namespace Dayone\Issuer;

class OCB_eGift {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\OCBServiceProvider');
        return 'OCB::ocb_egift';
    }

}