<?php
namespace Dayone\Issuer;

class HSBC {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\HSBCServiceProvider');
        return 'HSBC::index';
    }

}