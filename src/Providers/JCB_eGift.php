<?php
namespace Dayone\Issuer;

class JCB_eGift {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\JCBServiceProvider');
        return 'JCB::jcb_egift';
    }

}