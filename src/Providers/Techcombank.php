<?php
//namespace App\Issue\Techcombank\Providers;
namespace Dayone\Issuer;
class Techcombank {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\TechcombankServiceProvider');
        return 'Techcombank::index';
    }

}